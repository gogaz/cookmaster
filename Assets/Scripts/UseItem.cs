﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cookmaster
{
    public class UseItem : MonoBehaviour
    {
        GameObject usingItem;
        List<Collider> colliders;
        void Start()
        {
            colliders = new List<Collider>();
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (usingItem == null)
                {
                    foreach (var collider in colliders)
                    {
                        usingItem = collider.gameObject;
                        usingItem.GetComponent<UsableItem>().Use(this);
                        break;
                    }
                }
            }
        }

        void OnTriggerEnter(Collider other)
        {
            UsableItem pa = other.GetComponent<UsableItem>();
            if (pa != null)
            {
                colliders.Add(other);
                if (usingItem == null)
                {
                    pa.Hilight();
                }
            }
        }

        void OnTriggerExit(Collider other)
        {
            colliders.Remove(other);
            UsableItem pa = other.GetComponent<UsableItem>();
            if (pa != null)
                pa.Hilight(false);
        }
    }
}

