﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using cakeslice;

namespace cookmaster
{
    public class UsableItem : MonoBehaviour
    {

        Outline outline;
        // Use this for initialization
        void Start()
        {
            this.gameObject.AddComponent<Outline>();
            outline = gameObject.GetComponent<Outline>();
            outline.enabled = false;
            outline.color = 2;
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void Use(UseItem ui)
        {
            Hilight(false);
        }

        public void Hilight(bool active = true)
        {
            outline.enabled = active;
        }
    }
}
