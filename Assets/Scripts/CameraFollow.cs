﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject player;
    public float cameraHeight = 20.0f;

    private Vector3 rel;
    public void Start()
    {
        Vector3 initialPosition = transform.position;
        Vector3 playerPos = player.transform.position;
        rel = initialPosition - playerPos;
    }
    void Update()
    {
        Vector3 pos = player.transform.position;
        transform.position = pos + rel;
    }
}
