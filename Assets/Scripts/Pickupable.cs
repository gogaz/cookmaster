﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using cakeslice;

namespace cookmaster
{
    public class Pickupable : MonoBehaviour
    {
        Outline outline;
        // Use this for initialization
        void Start()
        {
            this.gameObject.AddComponent<Outline>();
            outline = gameObject.GetComponent<Outline>();
            outline.enabled = false;
            outline.color = 0;
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void PickUp()
        {
            Hilight(false);
            SetAllCollidersStatus(false);
        }

        public void Drop()
        {
            GetComponent<Rigidbody>().isKinematic = false;
            Hilight();
            SetAllCollidersStatus(true);
        }

        public void Hilight(bool active = true)
        {
            outline.enabled = active;
        }

        public void SetAllCollidersStatus(bool active)
        {
            foreach (Collider c in GetComponents<Collider>())
            {
                c.enabled = active;
            }
        }
    }
}

