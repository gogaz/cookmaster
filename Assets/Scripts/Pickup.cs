﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using cakeslice;

namespace cookmaster
{
    public class Pickup : MonoBehaviour
    {

        Rigidbody carriedObject = null;
        List<GameObject> colliders;
        void Start()
        {
            colliders = new List<GameObject>();
        }

        private void carry()
        {
            if (!carriedObject)
                return;
            carriedObject.isKinematic = true;
            carriedObject.transform.position = this.transform.position;
            carriedObject.transform.rotation = this.transform.rotation;
        }

        private void drop()
        {
            carriedObject.GetComponent<Pickupable>().Drop();
            carriedObject = null;

        }


        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (carriedObject == null)
                {
                    foreach (var collider in colliders)
                    {
                        carriedObject = collider.GetComponent<Rigidbody>();
                        carriedObject.GetComponent<Pickupable>().PickUp();
                        colliders.Remove(collider);
                        carry();
                        break;
                    }
                }
                else
                {
                    drop();
                }
            }
            else carry();
        }

        void OnTriggerEnter(Collider other)
        {
            Pickupable pa = other.GetComponent<Pickupable>();
            if (pa != null)
            {
                colliders.Add(other.gameObject);
                if (carriedObject == null)
                {
                    pa.Hilight();
                }
            }
        }

        void OnTriggerExit(Collider other)
        {
            colliders.Remove(other.gameObject);
            Pickupable pa = other.GetComponent<Pickupable>();
            if (pa != null)
                pa.Hilight(false);
        }
    }
}
