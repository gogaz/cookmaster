﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cookmaster
{
    public class MicroWave : MonoBehaviour
    {
        public int seconds;
        private GameObject slot;
        bool running;
        float timeLeft;
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            
            if (running)
            {
                timeLeft -= Time.deltaTime;

            }
        }
        
        // TODO:
        // Classe abstraite parente pour tout type d'objet: four/micro-onde/...
        // Ne pas afficher l'état de l'objet mais l'état du contenu -> stocker la temperature de l'ingredient dans l'ingredient
        // CookSpirit: script rattaché au player permettant de faire des actions en restant appuyé sur une touche par ex.
        private Color32 TimeLeftToColor()
        {
            if (timeLeft < seconds / 2)
            {
                return new Color32(255, 0, 0, 255);
            }
            else return new Color32(0, 255, 0, 255);
        }
    }
}

